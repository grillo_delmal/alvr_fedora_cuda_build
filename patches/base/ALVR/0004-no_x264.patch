From 04acbf0b57ae42fe165a61176e8cdef57d22fa9e Mon Sep 17 00:00:00 2001
From: Grillo del Mal <grillo@delmal.cl>
Date: Fri, 17 Jan 2025 16:11:23 -0300
Subject: [PATCH 4/5] no_x264

---
 alvr/server_openvr/build.rs                   |  49 -------
 .../cpp/platform/linux/EncodePipelineSW.cpp   | 125 +-----------------
 .../cpp/platform/linux/EncodePipelineSW.h     |  12 --
 alvr/xtask/src/dependencies.rs                |  76 -----------
 4 files changed, 3 insertions(+), 259 deletions(-)

diff --git a/alvr/server_openvr/build.rs b/alvr/server_openvr/build.rs
index 57a8de90..9bab253b 100644
--- a/alvr/server_openvr/build.rs
+++ b/alvr/server_openvr/build.rs
@@ -16,11 +16,6 @@ fn get_ffmpeg_path() -> PathBuf {
     }
 }
 
-#[cfg(all(target_os = "linux", feature = "gpl"))]
-fn get_linux_x264_path() -> PathBuf {
-    alvr_filesystem::deps_dir().join("linux/x264/alvr_build")
-}
-
 fn main() {
     let platform_name = env::var("CARGO_CFG_TARGET_OS").unwrap();
     let out_dir = PathBuf::from(env::var("OUT_DIR").unwrap());
@@ -90,47 +85,8 @@ fn main() {
         build.include(ffmpeg_path.join("include"));
     }
 
-    #[cfg(all(target_os = "linux", feature = "gpl"))]
-    {
-        let x264_path = get_linux_x264_path();
-
-        assert!(x264_path.join("include").exists());
-        build.include(x264_path.join("include"));
-    }
-
-    #[cfg(feature = "gpl")]
-    build.define("ALVR_GPL", None);
-
     build.compile("bindings");
 
-    #[cfg(all(target_os = "linux", feature = "gpl"))]
-    {
-        let x264_path = get_linux_x264_path();
-        let x264_lib_path = x264_path.join("lib");
-
-        println!(
-            "cargo:rustc-link-search=native={}",
-            x264_lib_path.to_string_lossy()
-        );
-
-        let x264_pkg_path = x264_lib_path.join("pkgconfig");
-        assert!(x264_pkg_path.exists());
-
-        let x264_pkg_path = x264_pkg_path.to_string_lossy().to_string();
-        env::set_var(
-            "PKG_CONFIG_PATH",
-            env::var("PKG_CONFIG_PATH").map_or(x264_pkg_path.clone(), |old| {
-                format!("{x264_pkg_path}:{old}")
-            }),
-        );
-        println!("cargo:rustc-link-lib=static=x264");
-
-        pkg_config::Config::new()
-            .statik(true)
-            .probe("x264")
-            .unwrap();
-    }
-
     // ffmpeg
     if gpl_or_linux {
         let ffmpeg_path = get_ffmpeg_path();
@@ -192,11 +148,6 @@ fn main() {
         pkg_config::Config::new().probe("vulkan").unwrap();
         pkg_config::Config::new().probe("openvr").unwrap();
 
-        #[cfg(not(feature = "gpl"))]
-        {
-            pkg_config::Config::new().probe("x264").unwrap();
-        }
-
         // fail build if there are undefined symbols in final library
         println!("cargo:rustc-cdylib-link-arg=-Wl,--no-undefined");
     }
diff --git a/alvr/server_openvr/cpp/platform/linux/EncodePipelineSW.cpp b/alvr/server_openvr/cpp/platform/linux/EncodePipelineSW.cpp
index 646a2db2..66cce662 100644
--- a/alvr/server_openvr/cpp/platform/linux/EncodePipelineSW.cpp
+++ b/alvr/server_openvr/cpp/platform/linux/EncodePipelineSW.cpp
@@ -1,144 +1,25 @@
 #include "EncodePipelineSW.h"
 
-#include <chrono>
-
 #include "FormatConverter.h"
 #include "alvr_server/Logger.h"
 #include "alvr_server/Settings.h"
 
-namespace {
-
-void x264_log(void*, int level, const char* fmt, va_list args) {
-    char buf[256];
-    vsnprintf(buf, sizeof(buf), fmt, args);
-    switch (level) {
-    case X264_LOG_ERROR:
-        Error("x264: %s", buf);
-        break;
-    case X264_LOG_WARNING:
-        Warn("x264: %s", buf);
-        break;
-    case X264_LOG_INFO:
-        Info("x264: %s", buf);
-        break;
-    case X264_LOG_DEBUG:
-        Debug("x264: %s", buf);
-        break;
-    default:
-        break;
-    }
-}
-
-}
-
 alvr::EncodePipelineSW::EncodePipelineSW(Renderer* render, uint32_t width, uint32_t height) {
-    const auto& settings = Settings::Instance();
-
-    x264_param_default_preset(&param, "ultrafast", "zerolatency");
-
-    param.pf_log = x264_log;
-    param.i_log_level = X264_LOG_INFO;
-
-    param.b_aud = 0;
-    param.b_cabac = settings.m_entropyCoding == ALVR_CABAC;
-    param.b_sliced_threads = true;
-    param.i_threads = settings.m_swThreadCount;
-    param.i_width = width;
-    param.i_height = height;
-    param.rc.i_rc_method = X264_RC_ABR;
-
-    switch (settings.m_h264Profile) {
-    case ALVR_H264_PROFILE_BASELINE:
-        x264_param_apply_profile(&param, "baseline");
-        break;
-    case ALVR_H264_PROFILE_MAIN:
-        x264_param_apply_profile(&param, "main");
-        break;
-    default:
-    case ALVR_H264_PROFILE_HIGH:
-        x264_param_apply_profile(&param, "high");
-        break;
-    }
-
-    auto params = FfiDynamicEncoderParams {};
-    params.updated = true;
-    params.bitrate_bps = 30'000'000;
-    params.framerate = Settings::Instance().m_refreshRate;
-    SetParams(params);
-
-    enc = x264_encoder_open(&param);
-    if (!enc) {
-        throw std::runtime_error("Failed to open encoder");
-    }
-
-    x264_picture_init(&picture);
-    picture.img.i_csp = X264_CSP_I420;
-    picture.img.i_plane = 3;
-
-    x264_picture_init(&picture_out);
-
-    rgbtoyuv = new RgbToYuv420(
-        render,
-        render->GetOutput().image,
-        render->GetOutput().imageInfo,
-        render->GetOutput().semaphore
-    );
+    throw std::runtime_error("No sw encoder implemented");
 }
 
 alvr::EncodePipelineSW::~EncodePipelineSW() {
-    if (rgbtoyuv) {
-        delete rgbtoyuv;
-    }
-    if (enc) {
-        x264_encoder_close(enc);
-    }
 }
 
 void alvr::EncodePipelineSW::PushFrame(uint64_t targetTimestampNs, bool idr) {
-    rgbtoyuv->Convert(picture.img.plane, picture.img.i_stride);
-    rgbtoyuv->Sync();
-    timestamp.cpu = std::chrono::duration_cast<std::chrono::nanoseconds>(
-                        std::chrono::steady_clock::now().time_since_epoch()
-    )
-                        .count();
-
-    picture.i_type = idr ? X264_TYPE_IDR : X264_TYPE_AUTO;
-    pts = picture.i_pts = targetTimestampNs;
-    is_idr = idr;
-
-    int nnal = 0;
-    nal_size = x264_encoder_encode(enc, &nal, &nnal, &picture, &picture_out);
-    if (nal_size < 0) {
-        throw std::runtime_error("x264 encoder_encode failed");
-    }
+    throw std::runtime_error("openh264 EncodeFrame failed");
 }
 
 bool alvr::EncodePipelineSW::GetEncoded(FramePacket& packet) {
-    if (!nal) {
-        return false;
-    }
-    packet.size = nal_size;
-    packet.data = nal[0].p_payload;
-    packet.pts = pts;
-    packet.isIDR = is_idr;
-    return packet.size > 0;
+    return false;
 }
 
 void alvr::EncodePipelineSW::SetParams(FfiDynamicEncoderParams params) {
-    if (!params.updated) {
-        return;
-    }
-    // x264 doesn't work well with adaptive bitrate/fps
-    param.i_fps_num = Settings::Instance().m_refreshRate;
-    param.i_fps_den = 1;
-    param.rc.i_bitrate
-        = params.bitrate_bps / 1'000 * 1.4; // needs higher value to hit target bitrate
-    param.rc.i_vbv_buffer_size = param.rc.i_bitrate / param.i_fps_num * 1.1;
-    param.rc.i_vbv_max_bitrate = param.rc.i_bitrate;
-    param.rc.f_vbv_buffer_init = 0.75;
-    if (enc) {
-        x264_encoder_reconfig(enc, &param);
-    }
 }
 
 int alvr::EncodePipelineSW::GetCodec() { return ALVR_CODEC_H264; }
diff --git a/alvr/server_openvr/cpp/platform/linux/EncodePipelineSW.h b/alvr/server_openvr/cpp/platform/linux/EncodePipelineSW.h
index 5bfb93af..9dd3cfd0 100644
--- a/alvr/server_openvr/cpp/platform/linux/EncodePipelineSW.h
+++ b/alvr/server_openvr/cpp/platform/linux/EncodePipelineSW.h
@@ -2,8 +2,6 @@
 
 #include "EncodePipeline.h"
 
-#include <x264.h>
-
 class FormatConverter;
 
 namespace alvr {
@@ -18,15 +16,5 @@ public:
     void SetParams(FfiDynamicEncoderParams params) override;
     int GetCodec() override;
 
-private:
-    x264_t* enc = nullptr;
-    x264_param_t param;
-    x264_picture_t picture;
-    x264_picture_t picture_out;
-    x264_nal_t* nal = nullptr;
-    int nal_size = 0;
-    int64_t pts = 0;
-    bool is_idr = false;
-    FormatConverter* rgbtoyuv = nullptr;
 };
 }
diff --git a/alvr/xtask/src/dependencies.rs b/alvr/xtask/src/dependencies.rs
index 60bab10b..7bdacf5f 100644
--- a/alvr/xtask/src/dependencies.rs
+++ b/alvr/xtask/src/dependencies.rs
@@ -17,46 +17,6 @@ pub fn choco_install(sh: &Shell, packages: &[&str]) -> Result<(), xshell::Error>
     .run()
 }
 
-pub fn prepare_x264_windows(deps_path: &Path) {
-    let sh = Shell::new().unwrap();
-
-    const VERSION: &str = "0.164";
-    const REVISION: usize = 3086;
-
-    let destination = deps_path.join("x264");
-
-    command::download_and_extract_zip(
-        &format!(
-            "{}/{VERSION}.r{REVISION}/libx264_{VERSION}.r{REVISION}_msvc16.zip",
-            "https://github.com/ShiftMediaProject/x264/releases/download",
-        ),
-        &destination,
-    )
-    .unwrap();
-
-    fs::write(
-        afs::deps_dir().join("x264.pc"),
-        format!(
-            r#"
-prefix={}
-exec_prefix=${{prefix}}/bin/x64
-libdir=${{prefix}}/lib/x64
-includedir=${{prefix}}/include
-
-Name: x264
-Description: x264 library
-Version: {VERSION}
-Libs: -L${{libdir}} -lx264
-Cflags: -I${{includedir}}
-"#,
-            destination.to_string_lossy().replace('\\', "/")
-        ),
-    )
-    .unwrap();
-
-    cmd!(sh, "setx PKG_CONFIG_PATH {deps_path}").run().unwrap();
-}
-
 pub fn prepare_ffmpeg_windows(deps_path: &Path) {
     command::download_and_extract_zip(
         &format!(
@@ -89,7 +49,6 @@ pub fn prepare_windows_deps(skip_admin_priv: bool) {
         .unwrap();
     }
 
-    prepare_x264_windows(&deps_path);
     prepare_ffmpeg_windows(&deps_path);
 }
 
@@ -100,43 +59,9 @@ pub fn prepare_linux_deps(enable_nvenc: bool) {
     sh.remove_path(&deps_path).ok();
     sh.create_dir(&deps_path).unwrap();
 
-    build_x264_linux(&deps_path);
     build_ffmpeg_linux(enable_nvenc, &deps_path);
 }
 
-pub fn build_x264_linux(deps_path: &Path) {
-    let sh = Shell::new().unwrap();
-
-    // x264 0.164
-    command::download_and_extract_tar(
-        "https://code.videolan.org/videolan/x264/-/archive/c196240409e4d7c01b47448d93b1f9683aaa7cf7/x264-c196240409e4d7c01b47448d93b1f9683aaa7cf7.tar.bz2",
-        deps_path,
-    )
-    .unwrap();
-
-    let final_path = deps_path.join("x264");
-
-    fs::rename(
-        deps_path.join("x264-c196240409e4d7c01b47448d93b1f9683aaa7cf7"),
-        &final_path,
-    )
-    .unwrap();
-
-    let flags = ["--enable-static", "--disable-cli", "--enable-pic"];
-
-    let install_prefix = format!("--prefix={}", final_path.join("alvr_build").display());
-
-    let _push_guard = sh.push_dir(final_path);
-
-    cmd!(sh, "./configure {install_prefix} {flags...}")
-        .run()
-        .unwrap();
-
-    let nproc = cmd!(sh, "nproc").read().unwrap();
-    cmd!(sh, "make -j{nproc}").run().unwrap();
-    cmd!(sh, "make install").run().unwrap();
-}
-
 pub fn build_ffmpeg_linux(enable_nvenc: bool, deps_path: &Path) {
     let sh = Shell::new().unwrap();
 
@@ -148,7 +73,6 @@ pub fn build_ffmpeg_linux(enable_nvenc: bool, deps_path: &Path) {
     fs::rename(deps_path.join("FFmpeg-n6.0"), &final_path).unwrap();
 
     let flags = [
-        "--enable-gpl",
         "--enable-version3",
         "--enable-static",
         "--disable-programs",
-- 
2.48.1

