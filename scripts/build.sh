#!/usr/bin/bash

set -ex

# Clean out folder
find /opt/out/ -mindepth 1 -maxdepth 1 -exec rm -r -- {} +

cd /opt
mkdir -p src

rsync -azh /opt/orig/ALVR/ /opt/src/ALVR/

if [ ! -z "$(ls -A ./patches 2> /dev/null)" ]; then
    pushd patches
    if [ ! -z "$(ls -A */ 2> /dev/null)" ]; then
        for d in */ ; do
            if [ -d /opt/src/${d} ]; then
                for p in ${d}*.patch; do
                    echo "patch /opt/patches/$p"
                    (cd /opt/src/${d}; patch -p1 < /opt/patches/$p)
                done
            fi
        done
    fi
    popd
fi

rsync -azh --ignore-existing /opt/cache/ /opt/src/ALVR/

echo "======== Build stuff ========"
export RUST_BACKTRACE=1
pushd src
pushd ALVR

#curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y
#source "$HOME/.cargo/env"
#cargo xtask check-format
#exit 1

## FFMPEG
curl -L -o ffmpeg.zip --url https://codeload.github.com/FFmpeg/FFmpeg/zip/n6.0
curl -L -o nv-codec-headers.zip --url https://github.com/FFmpeg/nv-codec-headers/archive/refs/tags/n12.1.14.0.zip

rm -rf ./openvr
cargo fetch --target "$(arch)-unknown-linux-gnu"
time \
cargo run \
    --frozen \
    --release \
    -p alvr_xtask \
    -- \
    prepare-deps \
    --platform linux
#cargo build \
#    --frozen \
#    --release \
#    -p alvr_server_openvr \
#    -p alvr_dashboard \
#    -p alvr_vulkan_layer \
#    -p alvr_vrcompositor_wrapper
time \
cargo xtask build-streamer
cargo xtask build-launcher
popd
popd

echo "======== Getting results ========"

# Install
rsync -azh /opt/src/ALVR/build/ /opt/out/ALVR/

# Clean cache
#find /opt/cache/ -mindepth 1 -maxdepth 1 -exec rm -r -- {} +
if [ -z "$(ls -A /opt/cache/ 2> /dev/null)" ]; then
    rsync -azh /opt/src/ALVR/ /opt/cache/
fi
