#!/usr/bin/bash

set -e

./prepare.sh

mkdir -p $(pwd)/build_out
mkdir -p $(pwd)/cache

podman unshare chown $UID:$UID -R $(pwd)/build_out
podman unshare chown $UID:$UID -R $(pwd)/cache

if [[ -z ${SRC_PATH} ]]; then
    if [[ ! -z ${NO_X264} ]]; then
        if [[ ! -z ${NO_CUDA} ]]; then
            podman run -ti --rm \
                -v $(pwd)/build_out:/opt/out/:Z \
                -v $(pwd)/cache:/opt/cache/:Z \
                -v $(pwd)/patches/base:/opt/patches/:ro,Z \
                -v $(pwd)/scripts:/opt/scripts/:ro,Z \
                -v $(pwd)/src/ALVR:/opt/orig/ALVR/:ro,Z \
                localhost/alvr_fedora:latest \
                /opt/scripts/build.sh
        else
            podman run -ti --rm \
                -v $(pwd)/build_out:/opt/out/:Z \
                -v $(pwd)/cache:/opt/cache/:Z \
                -v $(pwd)/patches/cuda:/opt/patches/:ro,Z \
                -v $(pwd)/scripts:/opt/scripts/:ro,Z \
                -v $(pwd)/src/ALVR:/opt/orig/ALVR/:ro,Z \
                localhost/alvr_fedora:cuda \
                /opt/scripts/build.sh
        fi
    else
        if [[ ! -z ${NO_CUDA} ]]; then
            podman run -ti --rm \
                -v $(pwd)/build_out:/opt/out/:Z \
                -v $(pwd)/cache:/opt/cache/:Z \
                -v $(pwd)/patches/x264:/opt/patches/:ro,Z \
                -v $(pwd)/scripts:/opt/scripts/:ro,Z \
                -v $(pwd)/src/ALVR:/opt/orig/ALVR/:ro,Z \
                localhost/alvr_fedora:latest \
                /opt/scripts/build.sh
        else
            podman run -ti --rm \
                -v $(pwd)/build_out:/opt/out/:Z \
                -v $(pwd)/cache:/opt/cache/:Z \
                -v $(pwd)/patches/x264_cuda:/opt/patches/:ro,Z \
                -v $(pwd)/scripts:/opt/scripts/:ro,Z \
                -v $(pwd)/src/ALVR:/opt/orig/ALVR/:ro,Z \
                localhost/alvr_fedora:cuda \
                /opt/scripts/build.sh
        fi
    fi
else
    if [[ ! -z ${NO_X264} ]]; then
        if [[ ! -z ${NO_CUDA} ]]; then
            podman run -ti --rm \
                -v $(pwd)/build_out:/opt/out/:Z \
                -v $(pwd)/cache:/opt/cache/:Z \
                -v $(pwd)/scripts:/opt/scripts/:ro,Z \
                -v ${SRC_PATH}:/opt/orig/ALVR/:ro,Z \
                localhost/alvr_fedora:latest \
                /opt/scripts/build.sh
        else
            podman run -ti --rm \
                -v $(pwd)/build_out:/opt/out/:Z \
                -v $(pwd)/cache:/opt/cache/:Z \
                -v $(pwd)/scripts:/opt/scripts/:ro,Z \
                -v ${SRC_PATH}:/opt/orig/ALVR/:ro,Z \
                localhost/alvr_fedora:cuda \
                /opt/scripts/build.sh
        fi
    else
        if [[ ! -z ${NO_CUDA} ]]; then
            podman run -ti --rm \
                -v $(pwd)/build_out:/opt/out/:Z \
                -v $(pwd)/cache:/opt/cache/:Z \
                -v $(pwd)/scripts:/opt/scripts/:ro,Z \
                -v ${SRC_PATH}:/opt/orig/ALVR/:ro,Z \
                localhost/alvr_fedora:latest \
                /opt/scripts/build.sh
        else
            podman run -ti --rm \
                -v $(pwd)/build_out:/opt/out/:Z \
                -v $(pwd)/cache:/opt/cache/:Z \
                -v $(pwd)/scripts:/opt/scripts/:ro,Z \
                -v ${SRC_PATH}:/opt/orig/ALVR/:ro,Z \
                localhost/alvr_fedora:cuda \
                /opt/scripts/build.sh
        fi
    fi
fi

podman unshare chown 0:0 -R $(pwd)/build_out
podman unshare chown 0:0 -R $(pwd)/cache
