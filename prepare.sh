#!/usr/bin/bash

set -e

if [[ ! -z ${NO_CUDA} ]]; then
    podman build \
        -f ./podman/alvr_fedora/Containerfile \
        -t registry.gitlab.com/grillo_delmal/alvr_fedora_build/alvr_fedora:latest \
        -t localhost/alvr_fedora:latest \
        .
else
    podman build \
        -f ./podman/fedora_cuda/Containerfile \
        -t registry.gitlab.com/grillo_delmal/alvr_fedora_build/fedora_cuda:41 \
        -t localhost/fedora_cuda:41 \
        .

    podman build \
        -f ./podman/alvr_fedora_cuda/Containerfile \
        -t registry.gitlab.com/grillo_delmal/alvr_fedora_build/alvr_fedora:cuda \
        -t localhost/alvr_fedora:cuda \
        .
fi

